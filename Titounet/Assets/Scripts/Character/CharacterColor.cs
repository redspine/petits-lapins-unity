﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class CharacterColor : MonoBehaviour
{
    public Color color = default;
    public List<MeshRenderer> renderers;
    private Color old_color = default;

    [MenuItem("Tools/UpdateRabbitColors")]
    public void UpdateColor() {
        if (color != old_color)
        {
            old_color = color;
            foreach (var renderer in renderers)
            {
                renderer.material.color = color;
            }
        }
    }
    
}
