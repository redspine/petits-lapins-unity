﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayEntry : MonoBehaviour
{
    public float delay;
    private float counter;
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<MeshRenderer>().enabled = false;
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;
        if(counter > delay)
        {
            GetComponentInChildren<MeshRenderer>().enabled = true;
            GetComponentInChildren<Animator>().SetTrigger("Spawn");
            Destroy(this);
        }
    }
}
